# Juego de la Vida de Conway

  

El Juego de la Vida de Conway es un autómata celular, creado por el matemático británico John Horton Conway en 1970. A pesar de sus reglas simples, el juego puede generar patrones complejos y sorprendentes. Es un ejemplo clásico de un sistema dinámico que exhibe comportamientos emergentes.

  

## Reglas del Juego:

  

1.  **Tablero**: El juego se desarrolla en un tablero bidimensional, donde cada celda puede estar viva o muerta. El tablero es infinito en todas direcciones, pero en la práctica se utiliza un tablero de tamaño finito.

  

2.  **Estado inicial** :El juego comienza con un patrón inicial de celulas vivas y muertas

  

3.  **Evolución**: Se consideraron dos juegos, el cual funcionan de la siguiente manera:

  

	-  **Juego de la vida de 2 especies**: En cada iteración, las células evolucionan de acuerdo con las siguientes reglas:

		1.  **Nacimiento**: Si una célula está muerta en la generación actual y tiene exactamente tres vecinos vivos, entonces la célula nace en la siguiente generación.

		2.  **Supervivencia**: Si una célula está viva en la generación actual y tiene dos o tres vecinos vivos en total, entonces la célula sobrevive en la siguiente generación. Además, si el número de vecinos de la otra especie es menor o igual a dos, la célula también sobrevive.

		3.  **Muerte**: Una célula viva muere en la siguiente generación si tiene menos de dos vecinos vivos o mas de tres vecinos vivos. Además, si el número de vecinos de la otra especie es mayor que dos, la célula también muere.

  

	-  **Juego de la vida de 2 especies con depredación**: En cada iteración, las células evolucionan de acuerdo con las siguientes reglas:

		1.  **Nacimiento**: Si una célula está muerta en la generación actual y tiene exactamente tres vecinos vivos, entonces la célula nace en la siguiente generación.

		2.  **Supervivencia**: Si una célula está viva en la generación actual y tiene dos o tres vecinos vivos en total, entonces la célula sobrevive en la siguiente generación. Además, si el número de vecinos de la otra especie es menor o igual a dos, la célula también sobrevive.

		3.  **Muerte**: Una célula viva muere en la siguiente generación si tiene menos de dos vecinos vivos o mas de tres vecinos vivos. Además, si el número de vecinos de la otra especie es mayor que dos, la célula también muere.
		4. **Predación**: Si una célula viva tiene exactamente dos vecinos vivos de la otra especie, entonces la célula es remplazada por una célula de la otra especie en la siguiente generación.

4. **Iteración**: Estas reglas se aplican simultáneamente a todas las células en cada generación, creando una nueva generación de células. Este proceso se repite para cada generación subsiguiente. 

## Implementación en Jupyter

El notebook en Jupyter proporcionado en este repositorio simula el juego de la vida de Conway. Utiliza matrices numpy para representar el tablero y las operaciones de matriz para aplicar las reglas de los dos juegos en cada iteración. También proporciona funciones para contar el número de vecinos de cada célula y determinar su destino en la siguiente generación. Además implementa histogramas de la evolución de las células.

## Uso del repositorio

Puedes clonar este repositorio y ejecutar el código en tu entorno de Jupyter para experimentar con diferentes patrones iniciales y observar la evolución del juego a lo largo de las generaciones. Además, puedes modificar el código y agregar nuevas características para personalizar la simulación del juego de la vida de Conway.

```sh
git clone https://gitlab.com/andres.rianoq/seguimiento2fiscom2.git
python3 -m venv env
source env/bin/activate
pip3 install -r requirements.txt
jupyter-notebook

```

## Contacto

Para cualquier pregunta o comentario, no dudes en ponerte en contacto con los autores:

-   **Andres Felipe Riaño Quintanilla**
    
    -   Email: andres.rianoq@udea.edu.co
-   **Julian Francisco Pinchao Ortiz**
    
    -   Email: jfrancisco.pinchao@udea.edu.co
-   **Santiago Julio Davila**
    
    -   Email: santiago.juliod@udea.edu.co
